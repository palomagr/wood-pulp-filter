Base Analysis developed by [Camron Blackburn.](https://gitlab.cba.mit.edu/camblackburn)

In Camron's material database it is possible to observe and compare the material properties from these natural fibers to artificial fibers. 

# Arauco Wood Pulp Manufacturer

## BKP - Radiata Pine Wood Pulp
Manufacturer page: [Arauco BKP](https://www.arauco.cl/na/marcas/woodpulp/arauco-bkp/)

<img src="images/BKP/bkp_raw.jpg" height = "200">

### physical measurements 
the pieces gold coated were imaged.

<img src="images/BKP/bkp_measured.jpg" height = "300">


### imaging
#### 80% compacted material
<img src="images/BKP/SEM/AraucoBKP_1_20nmAu_top_0002_raw.jpg" height="300">

<img src="images/BKP/SEM/AraucoBKP_1_20nmAu_top_0005_raw.jpg" height="300">

<img src="images/BKP/SEM/AraucoBKP_1_20nmAu_top_0006_raw.jpg" height="300">

#### 20% compacted material
top view from #6

<img src="images/BKP/SEM/AraucoBKP_6_20nmAu_top_0001_raw.jpg" height="300">

<img src="images/BKP/SEM/AraucoBKP_6_20nmAu_top_0003_raw.jpg" height="300">

side view from # 7
- you can see the layers that were more or less compacted. The main problem is accessing the right tools to cut the wood pulp layers. 

<img src="images/BKP/SEM/AraucoBKP_7_20nmAu_side_0001.jpg" height="300">

<img src="images/BKP/SEM/AraucoBKP_7_20nmAu_side_0003.jpg" height="300">

<img src="images/BKP/SEM/AraucoBKP_7_20nmAu_side_0004.jpg" height="300">

<img src="images/BKP/SEM/AraucoBKP_7_20nmAu_side_0007.jpg" height="300">



## EKP - Eucalyptus Wood Pulp
manufacturer page: [Arauco EKP](https://www.arauco.cl/aus-nz/marcas/woodpulp/arauco-ekp/)

### physical measurements
<img src="images/EKP/ekp_measured.jpg" height = "300">

### imaging
#### 80% compacted material
<img src="images/EKP/SEM/AraucoEKP_1_20nmAu_top_0009_raw.jpg" height="300">

<img src="images/EKP/SEM/AraucoEKP_1_20nmAu_top_0011.jpg" height="300">

<img src="images/EKP/SEM/AraucoEKP_1_20nmAu_top_0013.jpg" height="300">

<img src="images/EKP/SEM/AraucoEKP_1_20nmAu_top_0015.jpg" height="300">

#### 20% compacted material
<img src="images/EKP/SEM/AraucoEKP_8_20nmAu_top_0001.jpg" height="300">

<img src="images/EKP/SEM/AraucoEKP_8_20nmAu_top_0004.jpg" height="300">

there are small, leaf-like features with tiny holes . . 

<img src="images/EKP/SEM/AraucoEKP_8_20nmAu_top_0005.jpg" height="300">

<img src="images/EKP/SEM/AraucoEKP_8_20nmAu_top_0007.jpg" height="300">

<img src="images/EKP/SEM/AraucoEKP_8_20nmAu_top_0008.jpg" height="300">

## UKP - Unbleached Radiata Pine Pulp
manufacturer page: [Arauco UKP](https://www.arauco.cl/na/marcas/woodpulp/arauco-ukp/)

### physical measurements
<img src="images/UKP/ukp_measured.jpg" height = "300">

### imaging
#### 80% compacted material
<img src="images/UKP/SEM/AraucoUKP_1_20nmAu_top_0001.jpg" height="300">

<img src="images/UKP/SEM/AraucoUKP_1_20nmAu_top_0002.jpg" height="300">

<img src="images/UKP/SEM/AraucoUKP_1_20nmAu_top_0005.jpg" height="300">

<img src="images/UKP/SEM/AraucoUKP_1_20nmAu_top_0006.jpg" height="300">

<img src="images/UKP/SEM/AraucoUKP_1_20nmAu_top_0003.jpg" height="300">

#### 20% compacted material
<img src="images/UKP/SEM/AraucoUKP_6_20nmAu_top_0001.jpg" height="300">

<img src="images/UKP/SEM/AraucoUKP_6_20nmAu_top_0003.jpg" height="300">

<img src="images/UKP/SEM/AraucoUKP_6_20nmAu_top_0005.jpg" height="300">

<img src="images/UKP/SEM/AraucoUKP_6_20nmAu_top_0007.jpg" height="300">

<img src="images/UKP/SEM/AraucoUKP_6_20nmAu_top_0008.jpg" height="300">
